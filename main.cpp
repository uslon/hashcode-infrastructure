#include <iostream>


#include "projects/projects_manager.h"
#include "projects/projects_strategy.h"

void InitializeDirectory(int id, int argc, char** argv, const std::string& name,
                         const std::string& default_directory, std::string* destination) {
    if (argc <= id) {
        std::cout << "Using default " << name << " directory: " << default_directory << std::endl;
        *destination = default_directory;
    } else {
        *destination = std::string(argv[id]);
        std::cout << "Using following " << name << " directory: " << *destination << std::endl;
    }
}

int main(int argc, char** argv)
{
    std::string tests_directory;
    const std::string kDefaultTestsDirectory = "/home/amir/input/";
    InitializeDirectory(1, argc, argv, "tests", kDefaultTestsDirectory, &tests_directory);

    std::string results_directory;
    const std::string kDefaultResultsDirectory = "/home/amir/";
    InitializeDirectory(1, argc, argv, "results", kDefaultResultsDirectory, &results_directory);

    ProjectsManager Manager;

//    Manager.RegisterTask("b_better_start_small");
    Manager.RegisterTask("c_collaboration");
//    Manager.RegisterTask("d_dense_schedule");
//    Manager.RegisterTask("e_exceptional_skills");
//    Manager.RegisterTask("f_find_great_mentors");

    Manager.ReadTestsFromDirectory(tests_directory);

    Manager.RegisterStrategy(std::make_shared<ProjectsStrategy>());
    Manager.Run();

    Manager.PutAnswers(results_directory);
//    Manager.PutScores(results_directory);
}
