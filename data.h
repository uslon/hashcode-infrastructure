//
// Created by amir on 06.01.2022.
//

#ifndef HASHCODE__DATA_H
#define HASHCODE__DATA_H

#include <ostream>
#include <memory>

struct BaseSolution {
    virtual std::ostream& output(std::ostream& out) const = 0;
};

std::ostream& operator<<(std::ostream& out, const BaseSolution& solution);

struct BaseTestCase {
    std::shared_ptr<BaseSolution> best_solution;
    uint64_t best_score = 0;

    void Relax(const std::shared_ptr<BaseSolution>& solution, uint64_t score);

    virtual ~BaseTestCase();
};

#endif //HASHCODE__DATA_H
