//
// Created by amir on 06.01.2022.
//

#include "manager.h"

#include <iostream>

#include <filesystem>
#include <fstream>

#include <thread>

void BaseManager::RegisterStrategy(std::shared_ptr<BaseStrategy> strategy) {
    strategies_.push_back(strategy);
}

void BaseManager::RegisterTestCase(std::shared_ptr<BaseTestCase> test_case) {
    tests_.push_back(test_case);
}

void BaseManager::Run() {
    solutions_.resize(tests_.size());
    std::vector<std::thread> workers;
    workers.reserve(tests_.size());

    for (auto& test_case : tests_) {
        workers.emplace_back([this](std::shared_ptr<BaseTestCase>& test_case) {
            for (auto& strategy : strategies_) {
                strategy->Apply(test_case);
            }
        }, std::ref(test_case));
    }

    for (auto& worker : workers) {
        worker.join();
    }
}

void BaseManager::ReadTestsFromDirectory(const std::string& dir) {
    std::filesystem::path path(dir);

    for (auto& task_name : task_names_) {
        auto task_path = path;
        task_path.append(task_name + ".in.txt");

        RegisterTestCase(ReadTestFromFile(task_path));
    }
}

void BaseManager::ReadTestsFromDirectory(char *dir) {
    ReadTestsFromDirectory(std::string(dir));
}

void BaseManager::RegisterTask(const std::string &name) {
    task_names_.push_back(name);
}

void BaseManager::PutAnswers(const std::string &dir) {
    std::filesystem::path path(dir);

    for (size_t i = 0; i < task_names_.size(); ++i) {
        auto task_path = path;
        task_path.append(task_names_[i] + ".out.txt");

        std::ofstream out(task_path, std::ios_base::out);
        out << *tests_[i]->best_solution << std::endl;
    }
}

void BaseManager::PutScores(const std::string& dir) {
    std::filesystem::path path(dir);
    std::ofstream out(dir + "scores.txt");
    for (auto& test : tests_) {
        out << test->best_score << ' ';
    }
}
