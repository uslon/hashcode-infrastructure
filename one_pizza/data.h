//
// Created by amir on 14.02.2022.
//

#ifndef HASHCODE_ONE_PIZZA_DATA_H
#define HASHCODE_ONE_PIZZA_DATA_H

#include "../data.h"

#include <vector>
#include <unordered_set>

struct PizzaSolution : public BaseSolution
{
    explicit PizzaSolution(std::unordered_set<std::string>&& ingredients) : ingredients(std::move(ingredients)) {
    }

    std::ostream& output(std::ostream& out) const override;

    std::unordered_set<std::string> ingredients;
};

struct PizzaTestCase : public BaseTestCase
{
    std::vector<std::vector<std::string>> likes;
    std::vector<std::vector<std::string>> dislikes;
};

#endif //HASHCODE_ONE_PIZZA_DATA_H
