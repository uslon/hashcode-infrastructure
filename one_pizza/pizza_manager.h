//
// Created by amir on 14.02.2022.
//

#ifndef HASHCODE_ONE_PIZZA_PIZZA_MANAGER_H
#define HASHCODE_ONE_PIZZA_PIZZA_MANAGER_H

#include "../manager.h"


uint64_t
RunSolution(std::shared_ptr<BaseSolution> solution, std::shared_ptr<BaseTestCase> test_case);

class PizzaManager : public BaseManager
{
public:
    uint64_t
    RunSolution(std::shared_ptr<BaseSolution> solution, std::shared_ptr<BaseTestCase> test_case) override;

    std::shared_ptr<BaseTestCase> ReadTestFromFile(const std::filesystem::path& path) override;
};

#endif //HASHCODE_ONE_PIZZA_PIZZA_MANAGER_H
