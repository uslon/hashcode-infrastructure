//
// Created by amir on 14.02.2022.
//

#include "data.h"

std::ostream& PizzaSolution::output(std::ostream& out) const
{
    out << ingredients.size() << ' ';
    for (auto& ingredient : ingredients) {
        out << ingredient << ' ';
    }

    return out;
}
