//
// Created by amir on 14.02.2022.
//

#ifndef HASHCODE_ONE_PIZZA_PIZZA_STRATEGY_H
#define HASHCODE_ONE_PIZZA_PIZZA_STRATEGY_H

#include "../strategy.h"

#include <unordered_set>

struct PizzaStrategy : public RandomUsingStrategy
{
    PizzaStrategy() : PizzaStrategy(kDefaultIterations)
    {
    }

    explicit PizzaStrategy(int iterations) : kIterations(iterations)
    {
    }

    void
    Apply(std::shared_ptr<BaseTestCase> test_case) override;

    const int kIterations;

    static const int kDefaultIterations = 1024*128;

private:
    std::unordered_set<std::string> ChooseRandomIngredients(const std::unordered_set<std::string> &ingredients);
};
#endif //HASHCODE_ONE_PIZZA_PIZZA_STRATEGY_H
