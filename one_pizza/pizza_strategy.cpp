//
// Created by amir on 14.02.2022.
//

#include "pizza_strategy.h"

#include "data.h"
#include "pizza_manager.h"

#include <mutex>
#include <shared_mutex>
#include <thread>
#include <iostream>

std::unordered_set<std::string> GetAllIngredients(const std::shared_ptr<PizzaTestCase>& test_case) {
    std::unordered_set<std::string> ingredients;
    if (!test_case) {
        return ingredients;
    }
    
    for (auto& preferences : test_case->likes) {
        for (auto& ingredient : preferences) {
            ingredients.insert(ingredient);
        }
    }
    
    return ingredients;
}

std::unordered_set<std::string> ChooseRandomIngredients(
    const std::unordered_set<std::string>& ingredients, std::mt19937& rnd) {
    std::unordered_set<std::string> subset;
    for (auto& ingredient : ingredients) {
        if (rnd() % 3) {
            subset.insert(ingredient);
        }
    }
    
    return subset;
}

void Mine(std::mutex& mutex, const std::unordered_set<std::string>& ingredients,
          PizzaTestCase& test_case, int iterations, int id) {
    std::mt19937 rnd(id ^ 2821);

    PizzaTestCase local_test = test_case;

    for (int i = 0; i < iterations; ++i) {
        if (i % (4 * 474) == 0) {
            std::cout << "[" << id << "] " << i * 100 / iterations <<
            "% current max: " << local_test.best_score << std::endl;
        }

        auto solution = std::make_shared<PizzaSolution>(ChooseRandomIngredients(ingredients, rnd));
        local_test.Relax(solution, RunSolution(solution, std::make_shared<PizzaTestCase>(local_test)));
    }

    {
        std::lock_guard guard(mutex);
        test_case.Relax(local_test.best_solution, local_test.best_score);
    }
}

void PizzaStrategy::Apply(std::shared_ptr<BaseTestCase> test_case)
{
    auto pizza_test = std::dynamic_pointer_cast<PizzaTestCase>(test_case);
    if (!pizza_test) {
        return;
    }

    std::mutex mutex;
    auto ingredients = GetAllIngredients(pizza_test);
    int iterations = static_cast<int>(std::max(128lu, 22lu * 1024lu * 1024 / ingredients.size()));

    std::vector<std::thread> workers;
    const int kWorkers = 8;
    workers.reserve(kWorkers);
    for (int i = 0; i < kWorkers; ++i) {
        workers.emplace_back(Mine, std::ref(mutex), std::ref(ingredients), std::ref(*pizza_test), iterations, i);
    }

    for (auto& worker : workers) {
        worker.join();
    }
}
