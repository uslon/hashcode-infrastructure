//
// Created by amir on 14.02.2022.
//

#include "pizza_manager.h"

#include "data.h"

#include <fstream>
#include <iostream>

bool ContainsAll(const std::unordered_set<std::string>& set, std::vector<std::string>& ingredients) {
    for (auto& ingredient : ingredients) {
        if (!set.contains(ingredient)) {
            return false;
        }
    }
    return true;
}

bool ContainsNone(const std::unordered_set<std::string>& set, std::vector<std::string>& ingredients) {
    for (auto& ingredient : ingredients) {
        if (set.contains(ingredient)) {
            return false;
        }
    }
    return true;
}

uint64_t PizzaManager::RunSolution(std::shared_ptr<BaseSolution> base_solution,
                                   std::shared_ptr<BaseTestCase> base_test_case) {
    return RunSolution(base_solution, base_test_case);
}

std::vector<std::string> ReadIngredients(std::istream& in) {
    int m;
    in >> m;

    std::vector<std::string> ingredients(m);
    for (auto& ingredient : ingredients) {
        in >> ingredient;
    }

    return ingredients;
}

std::shared_ptr<BaseTestCase> PizzaManager::ReadTestFromFile(const std::filesystem::path &path)
{
    std::ifstream in(path);
    int n;
    in >> n;

    auto test =  std::make_shared<PizzaTestCase>();
    test->likes.reserve(n);
    test->dislikes.reserve(n);
    for (int i = 0; i < n; ++i) {
        test->likes.push_back(ReadIngredients(in));
        test->dislikes.push_back(ReadIngredients(in));
    }

    return test;
}
uint64_t
RunSolution(std::shared_ptr<BaseSolution> base_solution, std::shared_ptr<BaseTestCase> base_test_case)
{
    auto test_case = std::dynamic_pointer_cast<PizzaTestCase>(base_test_case);
    auto solution = std::dynamic_pointer_cast<PizzaSolution>(base_solution);
    if (!test_case) {
        std::cerr << "RunSolution error: got unexpected test case" << std::endl;
        return 0;
    }
    if (!solution) {
        std::cerr << "RunSolution error: got unexpected solution" << std::endl;
        return 0;
    }

    int cnt = 0;
    for (int i = 0; i < test_case->likes.size(); ++i) {
        cnt += ContainsAll(solution->ingredients, test_case->likes[i]) &&
            ContainsNone(solution->ingredients, test_case->dislikes[i]);
    }

    return cnt;
}
