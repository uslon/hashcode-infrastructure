//
// Created by amir on 24.02.2022.
//

#include "data.h"

std::ostream& ProjectsSolution::output(std::ostream &out) const {
    out << projects.size() << '\n';
    for (auto& project : projects) {
        out << project.name << '\n';
        for (auto& assignee : project.assignees) {
            out << assignee << ' ';
        }
        out << '\n';
    }

    return out;
}