//
// Created by amir on 24.02.2022.
//

#ifndef HASHCODE_PROJECTS_PROJECTS_MANAGER_H
#define HASHCODE_PROJECTS_PROJECTS_MANAGER_H

#include "../manager.h"

class ProjectsManager : public BaseManager
{
public:
    uint64_t
    RunSolution(std::shared_ptr<BaseSolution> solution, std::shared_ptr<BaseTestCase> test_case) override;

    std::shared_ptr<BaseTestCase> ReadTestFromFile(const std::filesystem::path& path) override;
};

#endif //HASHCODE_PROJECTS_PROJECTS_MANAGER_H
