//
// Created by amir on 24.02.2022.
//

#ifndef HASHCODE_PROJECTS_PROJECTS_STRATEGY_H
#define HASHCODE_PROJECTS_PROJECTS_STRATEGY_H

#include "../strategy.h"

class ProjectsStrategy : public BaseStrategy
{

    void Apply(std::shared_ptr<BaseTestCase> test_case) override;
};

#endif //HASHCODE_PROJECTS_PROJECTS_STRATEGY_H
