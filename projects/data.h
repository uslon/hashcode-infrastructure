//
// Created by amir on 24.02.2022.
//

#ifndef HASHCODE_PROJECTS_DATA_H
#define HASHCODE_PROJECTS_DATA_H

#include "../data.h"

#include <string>
#include <unordered_map>
#include <vector>

struct Project {
    std::vector<std::string> assignees;
    std::vector<std::string> roles;
    std::vector<int> levels;
    std::string name;

    int64_t duration = 0;
    int64_t score = 0;
    int64_t best_before = 0;
};

struct Contributor {
    std::unordered_map<std::string, int> skills;
    std::string name;
};

struct ProjectsSolution : public BaseSolution
{
    std::ostream& output(std::ostream& out) const override;

    std::vector<Project> projects;
};

struct ProjectsTestCase : public BaseTestCase {
    std::vector<Contributor> contributors;
    std::vector<Project> projects;

    std::unordered_map<std::string, int> contributors_index;
};

#endif //HASHCODE_PROJECTS_DATA_H
