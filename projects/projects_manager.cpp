//
// Created by amir on 24.02.2022.
//

#include "projects_manager.h"

#include "data.h"

#include <algorithm>
#include <fstream>
// #include <iostream>

uint64_t
ProjectsManager::RunSolution(std::shared_ptr<BaseSolution> solution, std::shared_ptr<BaseTestCase> test_case)
{
    return 0;
}

Contributor ReadContributor(std::ifstream& in) {
    Contributor contributor;
    in >> contributor.name;

    int n;
    in >> n;
    for (int i = 0; i < n; ++i) {
        std::string skill;
        int level;
        in >> skill >> level;
        contributor.skills[skill] = level;
    }

    return contributor;
}

Project ReadProject(std::ifstream& in) {
    Project project;
    in >> project.name >> project.duration >> project.score >> project.best_before;

    int n;
    in >> n;
    project.roles.resize(n);
    project.levels.resize(n);
    for (int i = 0; i < n; ++i) {
        in >> project.roles[i] >> project.levels[i];
    }

    return project;
}

std::shared_ptr<BaseTestCase>
ProjectsManager::ReadTestFromFile(const std::filesystem::path &path)
{
    std::ifstream in(path);
    int c, p;
    in >> c >> p;

    auto test = std::make_shared<ProjectsTestCase>();
    test->contributors.reserve(c);
    test->projects.reserve(p);

    for (int i = 0; i < c; ++i) {
        test->contributors.push_back(ReadContributor(in));
    }
    std::mt19937 rnd(3);
    std::shuffle(test->contributors.begin(), test->contributors.end(), rnd);

    for (int i = 0; i < c; ++i) {
        test->contributors_index[test->contributors.back().name] = i;
    }

    for (size_t i = 0; i < p; ++i) {
        test->projects.push_back(ReadProject(in));
    }

    return test;
}
