//
// Created by amir on 24.02.2022.
//

#include "projects_strategy.h"

#include "data.h"
#include "projects_manager.h"

#include <iostream>
#include <queue>
#include <thread>
#include <vector>
#include <unordered_set>

std::queue<Project> MoveToQueue(const std::vector<Project>& projects) {
    std::queue<Project> q;
    for (auto& p : projects) {
        q.push(p);
    }
    return q;
}

void UpdateSkills(std::shared_ptr<ProjectsTestCase>& test_case, const Project& project) {
    for (size_t project_id = 0; project_id < project.roles.size(); ++project_id) {
        const std::string& role = project.roles[project_id];
        int contributor_id = test_case->contributors_index[project.assignees[project_id]];
        int& contributor_level = test_case->contributors[contributor_id]
            .skills[role];

        if (contributor_level == project.levels[project_id]) {
            ++contributor_level;
            continue;
        }

        if (contributor_level + 1 == project.levels[project_id]) {
            for (size_t j = 0; j < project.roles.size(); ++j) {
                int mentor_id = test_case->contributors_index[project.assignees[j]];

                if (test_case->contributors[mentor_id].skills.count(role) &&
                    test_case->contributors[mentor_id].skills[role] >= project.levels[project_id]) {
                    ++contributor_level;
                    break;
                }
            }
        }
    }
}

bool AssignTeam(std::shared_ptr<ProjectsTestCase>& test_case, Project& cur, Project& project) {
    std::unordered_set<std::string> chosen;
    size_t old_size = 1;
    std::vector<bool> assigned(cur.roles.size());
    project.assignees.resize(cur.roles.size());

    while (old_size != chosen.size())
    {
        old_size = chosen.size();

        for (size_t i = 0; i < cur.roles.size(); ++i)
        {
            if (assigned[i]) {
                continue;
            }

            for (auto &contributor : test_case->contributors)
            {
                if (!chosen.count(contributor.name) &&
                    contributor.skills[cur.roles[i]] >= cur.levels[i])
                {
                    chosen.insert(contributor.name);
                    project.assignees[i] = (contributor.name);
                    assigned[i] = true;
                    break;
                }

                if (contributor.skills[cur.roles[i]] + 1 == cur.levels[i]) {
                    for (auto& assignee : chosen) {
                        int assignee_id = test_case->contributors_index[assignee];
                        if (test_case->contributors[assignee_id].skills[cur.roles[i]] >= cur.levels[i]) {
                            assigned[i] = true;
                            project.assignees[i] = contributor.name;
                            chosen.insert(contributor.name);
                            break;
                        }
                    }

                    if (assigned[i]) {
                        break;
                    }
                }
            }
        }

        if (cur.roles.size() == chosen.size()) {
            return true;
        }
    }

    return false;
}

void BuildGreedySchedule(std::shared_ptr<ProjectsTestCase>& test_case) {
    std::sort(test_case->projects.begin(), test_case->projects.end(),
              [](const Project& a, const Project& b) { return a.score > b.score
              || a.score == b.score && a.roles.size() < b.roles.size(); });

//    std::sort(test_case->projects.begin(), test_case->projects.end(),
//              [](const Project& a, const Project& b) { return a.roles.size() < b.roles.size()
//              || (a.roles.size() == b.roles.size() && a.score > b.score); });

//    std::sort(test_case->projects.begin(), test_case->projects.end(),
//              [](const Project& a, const Project& b) { return
//              a.score / (a.duration) > b.score / (b.duration); });

    std::queue<Project> queue = MoveToQueue(test_case->projects);

    auto best_solution = std::make_shared<ProjectsSolution>();

    size_t iteration = 0;
    const size_t kMaxIterations = 6 * 1024;

    while (iteration < kMaxIterations) {
        Project project;
        auto cur = queue.front();
        queue.pop();

        if (++iteration % 2197 == 0) {
            std::cout << '[' << iteration * 100 / kMaxIterations << "%]" << " thread: "
            << std::hash<std::thread::id>{}(std::this_thread::get_id()) % 10 << std::endl;
        }

        if (AssignTeam(test_case, cur, project)) {
            project.name = cur.name;
            best_solution->projects.push_back(project);
            UpdateSkills(test_case, project);
        } else {
            queue.push(cur);
        }
    }

    test_case->best_solution = best_solution;
}

void ProjectsStrategy::Apply(std::shared_ptr <BaseTestCase> test_case) {
    auto projects_test = std::dynamic_pointer_cast<ProjectsTestCase>(test_case);
    if (!projects_test) {
        return;
    }

    BuildGreedySchedule(projects_test);
}
