//
// Created by amir on 06.01.2022.
//

#ifndef HASHCODE__MANAGER_H
#define HASHCODE__MANAGER_H

#include "strategy.h"
#include "data.h"

#include <filesystem>
#include <memory>
#include <vector>

struct BaseStrategy;

class BaseManager
{
public:
    void RegisterStrategy(std::shared_ptr<BaseStrategy> strategy);
    void RegisterTestCase(std::shared_ptr<BaseTestCase> test_case);

    void RegisterTask(const std::string& name);

    void Run();

    void ReadTestsFromDirectory(char *dir);
    void ReadTestsFromDirectory(const std::string& dir);

    void PutAnswers(const std::string &dir);
    void PutScores(const std::string& dir);

    virtual uint64_t
    RunSolution(std::shared_ptr<BaseSolution> solution, std::shared_ptr<BaseTestCase> test_case) = 0;

private:

    virtual std::shared_ptr<BaseTestCase> ReadTestFromFile(const std::filesystem::path& path) = 0;

private:
    std::vector<std::shared_ptr<BaseStrategy>> strategies_;
    std::vector<std::shared_ptr<BaseTestCase>> tests_;

    std::vector<std::shared_ptr<BaseSolution>> solutions_;

    std::vector<std::string> task_names_;
};

#endif //HASHCODE__MANAGER_H
