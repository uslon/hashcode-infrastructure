//
// Created by amir on 06.01.2022.
//

#include "data.h"

void BaseTestCase::Relax(const std::shared_ptr<BaseSolution> &solution, uint64_t score) {
    if (score > best_score || !best_solution) {
        best_score = score;
        best_solution = solution;
    }
}

BaseTestCase::~BaseTestCase()
{
}

std::ostream& operator<<(std::ostream& out, const BaseSolution& solution) {
    return solution.output(out);
}
