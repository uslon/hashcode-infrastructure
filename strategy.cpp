//
// Created by amir on 06.01.2022.
//

#include "strategy.h"

uint32_t RandomUsingStrategy::Random() {
    return generator();
}

uint32_t RandomUsingStrategy::Random(uint32_t n) { // [0, n)
    return generator() % n;
}

uint32_t RandomUsingStrategy::Random(uint32_t l, uint32_t r) { // [l, r)
    return generator() % (r - l) + l;
}
