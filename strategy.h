//
// Created by amir on 06.01.2022.
//

#ifndef HASHCODE__STRATEGY_H
#define HASHCODE__STRATEGY_H

#include "data.h"
#include "manager.h"

#include <memory>
#include <random>

struct BaseStrategy
{
    virtual void
    Apply(std::shared_ptr<BaseTestCase>) = 0;
};

struct RandomUsingStrategy : public BaseStrategy
{
    RandomUsingStrategy() : RandomUsingStrategy(0) {
    }

    explicit RandomUsingStrategy(int seed) : generator(seed) {
    }

    uint32_t Random();

    uint32_t Random(uint32_t n); // [l, r)

    uint32_t Random(uint32_t l, uint32_t r); // [l, r)

private:
    std::mt19937 generator;
};

#endif //HASHCODE__STRATEGY_H
